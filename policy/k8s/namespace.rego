package k8s.namespace

import data.stage

deny_namespace_incorrect_prefix[msg] {
    namespace := input.metadata.namespace
    stageName := data.stage.name
    stagePrefix := concat("",[ stageName, "-"])
    not startswith(namespace, stagePrefix)
    msg := sprintf("Namespace %s must be prefixed with stage prefix: %s.", [ namespace, stagePrefix])
}
