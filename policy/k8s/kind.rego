package k8s.kind

import data.stage

deny_kind_is_not_allowed[msg] {
    kind := input.kind
    all({
        allowed | allowed := data.stage.allowed_kinds[_]
        allowed == kind
    })
    msg := sprintf("Kind %s is not one of the allowed kinds: %s", [ kind, data.stage.allowed_kinds ])
}
