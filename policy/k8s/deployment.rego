package k8s.kind.deployment

import data.stage

deny_deployment_missing_label[msg] {
    is_deployment
    name := input.metadata.name
    all({
       required | required := data.stage.required_labels["Deployment"][_]
       input.metadata.labels[ required ]
    })
    msg := sprintf("Deployment %s must be qualified with a label: %s", [ name , data.stage.required_labels["Deployment"] ])
}

is_deployment {
   input.kind == "Deployment"
}
